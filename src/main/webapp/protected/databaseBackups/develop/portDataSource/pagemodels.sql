INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('service','Service Page','<frames>
	<frame pos="0">
		<descr>Sample Frame</descr>
	</frame>
</frames>',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title><@wp.currentPage param="title" /></title>
</head>
<body>
<h1><@wp.currentPage param="title" /></h1>
<a href="<@wp.url page="homepage" />" >Home</a><br>
<div><@wp.show frame=0 /></div>
</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('home','Home Page',NULL,NULL,NULL);
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('inspinia','inspinia','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>main</descr>
		<sketch x1="0" y1="0" x2="11" y2="11" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            <@wp.currentPage param="title" /> - <@wp.i18n key="PORTAL_TITLE" />
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="<@wp.info key="systemParam" paramName="applicationBaseURL" />
              favicon.png" type="image/png" />
              <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
              <!--[if lt IE 9]><#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            <@wp.currentPage param="title" /> - <@wp.i18n key="PORTAL_TITLE" />
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="<@wp.info key="systemParam" paramName="applicationBaseURL" />
              favicon.png" type="image/png" />
              <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
              <!--[if lt IE 9]>
              <script src="<@wp.resourceURL />static/js/entando-misc-html5-essentials/html5shiv.js"></script>
              <![endif]-->
              <@c.import url="/WEB-INF/aps/jsp/models/inc/content_inline_editing.jsp" />
              <@c.import url="/WEB-INF/aps/jsp/models/inc/header-inclusions.jsp" />
              <@c.import url="/WEB-INF/aps/jsp/models/inc/flash_toast.jsp" />
              <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body class="pace-done">
    <div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div></div>

                <@wp.show  frame=0 />

</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('inspinia-multiple-widget','inspinia-multiple-widget','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>header</descr>
		<sketch x1="0" y1="0" x2="11" y2="1" />
	</frame>
	<frame pos="1">
		<descr>content row 1 left</descr>
		<sketch x1="0" y1="2" x2="1" y2="3" />
	</frame>
	<frame pos="2">
		<descr>content row 1 main</descr>
		<sketch x1="2" y1="2" x2="9" y2="3" />
	</frame>
	<frame pos="3">
		<descr>content row 1 right</descr>
		<sketch x1="10" y1="2" x2="11" y2="3" />
	</frame>
	<frame pos="4">
		<descr>content row 2 left</descr>
		<sketch x1="0" y1="4" x2="1" y2="5" />
	</frame>
	<frame pos="5">
		<descr>content row 2 main 1</descr>
		<sketch x1="2" y1="4" x2="6" y2="5" />
	</frame>
	<frame pos="6">
		<descr>content row 2 main 2</descr>
		<sketch x1="7" y1="4" x2="9" y2="5" />
	</frame>
	<frame pos="7">
		<descr>content row 2 right</descr>
		<sketch x1="10" y1="4" x2="11" y2="5" />
	</frame>
	<frame pos="8">
		<descr>content row 3 left</descr>
		<sketch x1="0" y1="6" x2="1" y2="7" />
	</frame>
	<frame pos="9">
		<descr>content row 3 main 1</descr>
		<sketch x1="2" y1="6" x2="6" y2="7" />
	</frame>
	<frame pos="10">
		<descr>content row 3 main 2</descr>
		<sketch x1="7" y1="6" x2="9" y2="7" />
	</frame>
	<frame pos="11">
		<descr>content row 3 right</descr>
		<sketch x1="10" y1="6" x2="11" y2="7" />
	</frame>
	<frame pos="12">
		<descr>content row 4 left</descr>
		<sketch x1="0" y1="8" x2="1" y2="9" />
	</frame>
	<frame pos="13">
		<descr>content row 4 main</descr>
		<sketch x1="2" y1="8" x2="9" y2="9" />
	</frame>
	<frame pos="14">
		<descr>content row 4 right</descr>
		<sketch x1="10" y1="8" x2="11" y2="9" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            <@wp.currentPage param="title" /> - <@wp.i18n key="PORTAL_TITLE" />
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        
        <style>@import url(https://fonts.googleapis.com/css?family=Roboto);</style>
        <link rel="icon" href="<@wp.info key="systemParam" paramName="applicationBaseURL" />
              favicon.png" type="image/png" />
              
        <link rel="stylesheet" href="<@wp.resourceURL />static/css/styles/style.css" rel="stylesheet">
        <link rel="stylesheet" href="<@wp.resourceURL />static/css/styles/css/yilicss.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
      crossorigin="anonymous">
         <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
      crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ"
      crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
      crossorigin="anonymous"></script> 
      
      
    </head>
    <body>
        <div id="wrapper" class="gray-bg">
            <div class="row border-bottom white-bg">
                <@wp.show frame=0 />
            </div>
            <div class="wrapper wrapper-content">
            
                    <div class="row">
                        <div class="col-md-2">
                            <@wp.show frame=1 />
                        </div>
                        <div class="col-md-8">
                            <@wp.show frame=2 />
                        </div>
                        <div class="col-md-2">
                            <@wp.show frame=3 />
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-2">
                            <@wp.show frame=4 />
                        </div>
                        <div class="col-md-5">
                            <@wp.show frame=5 />
                        </div>
                        <div class="col-md-3">
                            <@wp.show frame=6 />
                        </div>
                        <div class="col-md-2">
                            <@wp.show frame=7 />
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-2">
                            <@wp.show frame=8 />
                        </div>
                        <div class="col-md-5">
                            <@wp.show frame=9 />
                        </div>
                        <div class="col-md-3">
                            <@wp.show frame=10 />
                        </div>
                        <div class="col-md-2">
                            <@wp.show frame=11 />
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-lg-2">
                            <@wp.show frame=12 />
                        </div>
                        <div class="col-lg-8">
                            <@wp.show frame=13 />
                        </div>
                        <div class="col-lg-2">
                            <@wp.show frame=14 />
                        </div>
                    </div>
                    
                </div>
            </div>
    
    </body>
</html>');
