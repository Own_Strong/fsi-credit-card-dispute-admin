INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('landing',0,'fsi-credit-card-dispute-customer',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('admin_home',0,'fsi-ccd-admin-js-navbar',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('admin_home',2,'fsi-ccd-admin-js-greeting',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('admin_home',5,'fsi-ccd-admin-js-progress',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('admin_home',10,'fsi-ccd-admin-js-avg-clients',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('admin_home',6,'fsi-ccd-admin-js-avg-review-time',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('admin_home',9,'fsi-ccd-admin-js-performance',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('admin_home',13,'fsi-ccd-admin-js-worklist',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('admin_task_detail',0,'fsi-ccd-admin-js-navbar',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('admin_task_detail',2,'fsi-ccd-admin-js-customer-details',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('admin_task_detail',13,'fsi-ccd-admin-js-dispute-details',NULL);
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('sample',5,'bpm-case-progress-status','<?xml version="1.0" encoding="UTF-8"?>
<properties><property key="channel">1</property><property key="frontEndMilestonesData">{"container-id":"credit-dispute-case_1.0-SNAPSHOT","name":"FraudDispute","knowledge-source-id":"1","case-id-prefix":"FR","stages":[],"id":"CreditCardDisputeCase.FraudDispute","milestones":[{"milestone-id":"_5DF9B265-4140-42B1-B34B-2A85ED6DBFBF","milestone-name":"Milestone 1: Automated Chargeback","milestone-mandatory":false,"visible":true,"percentage":30},{"milestone-id":"_BF6327BB-57ED-4316-BEBB-CC0B43F4DE13","milestone-name":"Milestone 2: Manual Chargeback","milestone-mandatory":false,"visible":true,"percentage":40},{"milestone-id":"_AA3A4A87-9512-40CD-A573-D2913D67FBA4","milestone-name":"Milestone 3: Credit Account","milestone-mandatory":false,"visible":true,"percentage":30}],"ui":{"progress-bar-type":"basic","additionalSettings":[]},"version":"1.0"}</property></properties>
');
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('sample',10,'bpm-case-chart','<?xml version="1.0" encoding="UTF-8"?>
<properties><property key="channel">1</property></properties>
');
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('sample',6,'bpm-case-comments','<?xml version="1.0" encoding="UTF-8"?>
<properties><property key="channel">1</property></properties>
');
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('sample',2,'bpm-case-details','<?xml version="1.0" encoding="UTF-8"?>
<properties><property key="channel">1</property></properties>
');
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('sample',9,'bpm-case-file','<?xml version="1.0" encoding="UTF-8"?>
<properties><property key="channel">1</property></properties>
');
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('sample',8,'bpm-case-roles','<?xml version="1.0" encoding="UTF-8"?>
<properties><property key="channel">1</property></properties>
');
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('sample',13,'bpm-process-diagram','<?xml version="1.0" encoding="UTF-8"?>
<properties><property key="channel">1</property></properties>
');
INSERT INTO widgetconfig_draft (pagecode,framepos,widgetcode,config) VALUES ('sample',0,'bpm-case-instance-selector','<?xml version="1.0" encoding="UTF-8"?>
<properties><property key="channel">1</property><property key="frontEndCaseData">{"container-id":"credit-dispute-case_1.0-SNAPSHOT","knowledge-source-id":"1"}</property></properties>
');
